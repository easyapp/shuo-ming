# README #

一起共同工作在同一個專案

### 步驟 ###

* 安裝
* 指令

### 需要安裝的軟體 ###

* 安裝xcode tools command [https://roidjim.wordpress.com/2019/10/09/mac-os-xcode-command-line-tools-install/]
* 安裝 homebrew [https://brew.sh/index_zh-tw]
* 安裝git [https://git-scm.com/]
* source tree [https://www.sourcetreeapp.com/]
* php  指令 brew install homebrew/php/php71
* composer brew install homebrew/php/composer
* 安裝 html 編輯器 [https://www.jetbrains.com/phpstorm/download/]

### 指令 ###

* cd 到專案目錄
* 安裝需要引用的lib $ composer install
* 本機測試 php -S localhost:8002

